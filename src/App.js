import React from 'react';
import './App.css';
import MainPage from "./pages/Main/MainLayout/MainLayout";
function App() {
  return (
    <div className="App">
      <MainPage />
    </div>
  );
}

export default App;
